function userReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_USER':
            return action.user
        case 'EDIT_USER':
            return {
                ...state,
                ...action.payload
            }
        
            
        default:
            return state
    }
}

// function usersReducer(state = [], action) {
//     switch (action.type) {
//         case 'ADD_USER':
//             return [...state, userReducer(null, action)]
//         case 'EDIT_USER':
//             return state.map((each, index) => {
//                 if (each.email === action.email) {
//                     return userReducer(each, index)
//                 }
//                 return each
//             })
//         default:
//             return state
//     }
// }

export default userReducer
