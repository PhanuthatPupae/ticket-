import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";

import axios from "axios";
import { connect } from "react-redux";
import { Button, WhiteSpace, WingBlank, Icon } from "@ant-design/react-native";

const DissmissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class Login extends Component {
  goMovie = () => {
    this.props.history.push("/main", this.state);
  };

  goRegisterPage = () => {
    this.props.history.push("/Register");
  };
  state = {
    email: "",
    password: ""
  };

  onSignin = () => {
    axios({
      url: "https://zenon.onthewifi.com/ticGo/users/login?email",
      method: "post",
      data: {
        // email:'poopae@gmail.com',
        // password:'1234'

        email: this.state.email,
        password: this.state.password
      }
    })
      .then(res => {
        const { data } = res;
        // const { user } = data;
        this.props.addUser(data);

        this.props.history.push("/main");
      })
      .catch(e => {
        console.log("error " + e);
        alert("Email password invalid");
      });
  };

  render() {
    return (
      <DissmissKeyboard>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <View style={styles.container}>
            <View style={styles.logoContainer}>
              <Image
                style={{ width: 150, height: 90 }}
                source={require("../Img/Logo.jpg")}
              />
              <Text style={styles.text}>Movie Ticket</Text>
              <View>
                <TextInput
                  style={styles.inputText}
                  placeholder="Emai or username"
                  placeholderTextColor="gray"
                  onChangeText={email => this.setState({ email })}
                />
               

                <TextInput
                  style={styles.inputText}
                  placeholder="Password"
                  placeholderTextColor="gray"
                  secureTextEntry={true}
                  onChangeText={password => this.setState({ password })}
                />
              </View>
              <View style={styles.row}>
                <View style={styles.buttonContainer}>
                  <Button type="primary" onPress={()=> this.onSignin()}>
                    Sign in
                  </Button>
                </View>
                <View style={styles.buttonContainer}>
                  <Button
                    type="primary"
                    onPress={() =>this.goRegisterPage()}
                  >
                    Sign up
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </DissmissKeyboard>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  text: {
    textAlign: "center",
    fontSize: 20,
    color: "#1E90FF"
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  logo: {
    width: 10,
    height: 50,
    borderRadius: 50
  },
  row: {
    flexDirection: "row",
    paddingVertical: 20
  },
  buttonContainer: {
    marginHorizontal: 10
  },
  inputText: {
    backgroundColor: "#DCDCDC",
    borderRadius: 15,
    height: 40,
    width: 250,
    paddingLeft: 45,
    marginTop: 20
  }
});
const mapDidpatchToProps = (dispatch) => {
  return {
      addUser: (user) => {
          dispatch({
              type: 'ADD_USER',
              user: user
          })
      }
  }
}


export default connect(
  null,
  mapDidpatchToProps
)(Login);
