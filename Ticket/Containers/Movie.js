import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  Button
} from "react-native";
import { connect } from "react-redux";
import axios from "axios";

import Modal from "./Modal";
var screen = Dimensions.get("window");
let res = [];

class movies extends Component {
  state = {
    items: [],
    MovieStartTime: {
      cinema: {},
      endDateTime: "",
      movie: {},
      seats: [],
      startDateTime: "",
      subtitle: "",
      _id: ""
    },
    date: new Date(),
    res: []
  };

  getAllShowTime = async () => {
    try {
      const allShowtimes = await axios.get(
        `https://zenon.onthewifi.com/ticGo/movies/showtimes/`,
        {
          params: {
            date: this.state.date.getTime()
          }
        }
      );
      // console.log("allShowtimes data", allShowtimes.data);

      const clickedMovieStartTime = allShowtimes.data.filter(ele => {
        // console.log("ele : ", ele.startDateTime);
        // console.log("ele : ", ele.movie._id);
        // console.log("this.props.Movies.poopae._id", this.props.Movies.poopae._id);

        return this.props.Movies.poopae._id === ele.movie._id;
      });

      this.setState({ MovieStartTime: clickedMovieStartTime });

      this.props.showTime(this.state.MovieStartTime);
      // console.log(this.props.Movies.all)
      // this.props.all(this.state.MovieStartTime)
      this.props.all(clickedMovieStartTime);
      console.log("clickedMovieStartTime 555+", this.state.MovieStartTime);

      console.log("state", this.state.MovieStartTime[0].startDateTime);

      // const length = this.state.MovieStartTime.length;
      // const dateNow = new Date().getTime();

      // let y = [] = new date(this.state.MovieStartTime[i].startDateTime)
      // for (i = 0; length >= i; i++) {
      //   console.log("state ++", this.state.MovieStartTime[i].startDateTime);
      //   // const dateNow = new Date().getTime();
      //   if (this.state.MovieStartTime[i].startDateTime >= dateNow) {

      //     res.push(this.state.MovieStartTime[i].startDateTime)
      //     console.log("res +++++",res);

      //     const abc = new Date().getHours();

      //     console.log("abc", abc);
      //     console.log("abc tome ", this.state.time);
      //   } else {
      //     console.log("eror");
      //   }
      //   console.log("resout loop",res);
      // }
    } catch (error) {
      console.log(error.response);
    }
  };

  getMovies = () => {
    axios
      .get("https://zenon.onthewifi.com/ticGo/movies")

      .then(items => {
        console.log("respose:", items);

        this.setState({ items: items.data });
      })
      .catch(e => {
        console.log("error 555 sus", e);
      });
  };

  clickMovie = itemData => {
    this.props.onClickMovie(itemData);
    this.getAllShowTime();
  };
  componentDidMount() {
    this.getMovies();
  }

  test = () => {
    console.log("state ti", this.state.time);
    // console.log("state modalVisible" ,this.state.modalVisible);
    // console.log("pro", this.props.Mo.modalVisible);
    // console.log("time", this.props.Movies.poopae._id);
    console.log("state +", this.state.MovieStartTime);
    if (
      this.state.MovieStartTime.startDateTime !==
      this.state.MovieStartTime.startDateTime
    ) {
      return this.state.MovieStartTime.startDateTime;
    }
  };

  render() {
  
    return (
      <View style={styles.container}>
        <Text
          style={{
            backgroundColor: "#1E90FF",
            padding: 15,
            color: "white",
            fontSize: 20,
            textAlign: "center"
          }}
        >
          Movie
        </Text>

        <FlatList
          data={this.state.items}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  this.clickMovie(item);
                }}
              >
                <View style={{ marginHorizontal: 15, marginVertical: 20 }}>
                  <Image
                    style={styles.imageThumbnail}
                    source={{ uri: item.image }}
                    resizeMode="cover"
                  />
                  <Text
                     numberOfLines={8}
                    style={{ marginVertical: 5, textAlign: "center" }}
                  >
                    {item.name}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          }}
          numColumns={2}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    Movies: state.Movies
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onClickMovie: item =>
      dispatch({
        type: "CLICK_MOVIE",
        payload: item
      }),
    onDismissDialog: items =>
      dispatch({
        type: "DISMISS_DIALOG"
      }),
    showTime: timeAllCinema =>
      dispatch({
        type: "SHOW_TIME",
        showTime: timeAllCinema
      })
  };
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    flex: 1,
    backgroundColor:"#F8F8FF"

   
  },
  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    height: 300,
    width: 150,
    borderRadius: 15
  },
  image: {
    height: 150,
    width: 250,
    borderRadius: 15
  },
  modal: {
    justifyContent: "center",
    alignItems: "center"
  },

  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  modal3: {
    height: 300,
    width: 300
  },
  modal4: {
    height: 500
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(movies);
