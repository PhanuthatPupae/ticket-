import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ImageBackground,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";

import axios from "axios";
import { connect } from "react-redux";
import { Button, WhiteSpace, WingBlank, Icon } from "@ant-design/react-native";

const DissmissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class editprofile extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: ""
  };
  Cancel = () =>{
    this.props.history.push("/profile")
  }

  onSave = () => {
    
    axios({
      url: "https://zenon.onthewifi.com/ticGo/users",
      method: "put",
      headers: {
        Authorization: `Bearer ${this.props.user.user.token}`
      },
      data: {
        firstName: this.state.firstName,
        lastName: this.state.lastName
      }
    })
      .then(() => {
        return axios.get("https://zenon.onthewifi.com/ticGo/users", {
          headers: {
            Authorization: `Bearer ${this.props.user.user.token}`
          }
        });
      })
      .then(response => {
        console.log("", response.data);
        console.log("5555", this.props.onEdituser);
        this.props.onEdituser(response.data);
        this.props.history.push("/profile");
      })
      .catch(e => {
        console.log("error ", e.response);
      });
  };

  render() {
    return (
      <DissmissKeyboard>
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <View style={styles.container}>
            <Text
              style={{
                backgroundColor: "#1E90FF",
                padding: 15,
                color: "white",
                fontSize: 20,
                textAlign: "center"
              }}
            >
              Edit Profile
            </Text>
            <View style={styles.logoContainer}>
              <View>
                <Text>First name :</Text>
                <TextInput
                  style={styles.inputText}
                  placeholder="Enter firstname"
                  placeholderTextColor="gray"
                  onChangeText={firstName => this.setState({ firstName })}
                />
                <Text style={{marginTop:20}}>Last name :</Text>
                <TextInput
                  style={styles.inputText}
                  placeholder="Enter lastname"
                  placeholderTextColor="gray"
                  onChangeText={lastName => this.setState({ lastName })}
                />
              </View>
              <View style={styles.row}>
                <View style={styles.buttonContainer}>
                  <Button
                  style={{marginHorizontal:10}}
                    type="primary"
                    onPress={() => {this.onSave()
                    
                    }}
                  >
                    Save
                  </Button>
                  <Button
                    type="warning"
                    onPress={() => {
                      this.Cancel();
                    }}
                  >
                    Cancel
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </DissmissKeyboard>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.userone
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onEdituser: user =>
      dispatch({
        type: "EDIT_USER",
        payload: user
      })
  };
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white"
  },
  text: {
    textAlign: "center",
    fontSize: 24,
    color: "#1E90FF"
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  logo: {
    width: 10,
    height: 50,
    borderRadius: 50
  },
  row: {
    flexDirection: "row",
    paddingVertical: 20
  },
  buttonContainer: {
    marginHorizontal: 10,
    flexDirection:"row"
  },
  inputText: {
    backgroundColor: "#DCDCDC",
    borderRadius: 15,
    height: 40,
    width: 300,
    paddingLeft: 45,
    marginTop: 20
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(editprofile);
