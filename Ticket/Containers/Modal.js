import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  StyleSheet,
  View,
  FlatList,
  Modal,
  Image,
  TouchableOpacity,
  Alert
} from "react-native";
import { withRouter } from "react-router";
import { Button, WingBlank, Icon } from "@ant-design/react-native";
import axios from "axios";

const mapStateToProps = state => {
  return {
    Movies: state.Movies,
    timeMovie: state.timeMovie
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onClickMovie: items =>
      dispatch({
        type: "CLICK_MOVIE",
        payload: items
      }),
    onDismissDialog: () =>
      dispatch({
        type: "DISMISS_DIALOG"
      })
  };
};

class Modaly extends Component {
  state = {
    showTimes: []
  };

  test = () => {
    console.log("", this.props.timeMovie.movie);
    console.log("ss", this.props.timeMovie.showTime[0].startDateTime);
    console.log("this prosp", this.props);
  };

  goHistory = item => {
    this.props.history.push("/seat", { time: item });
    this.props.onDismissDialog();
  };

  render() {
    return (
      <View style={styles.modal3}>
        <Modal
          presentationStyle="formSheet"
          animationType="slide"
          transparent={false}
          visible={this.props.Movies.modalVisible}
          onRequestClose={() => {
            this.props.onDismissDialog();
            Alert.alert("Modal has been closed.");
          }}
        >
          <View style={{ marginHorizontal: 25, marginVertical: 20, flex: 1 }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Image
                style={styles.imageThumbnail}
                source={{ uri: this.props.Movies.poopae.image }}
                resizeMode="cover"
              />
            </View>

            <Text style={{ marginTop: 10, fontSize: 16 }}>
              เรื่อง : {this.props.Movies.poopae.name}
            </Text>
            <Text style={{ marginTop: 10, fontSize: 16 }}>
              ระยะเวลา : {this.props.Movies.poopae.duration} นาที
            </Text>
            <View
              style={{
                borderBottomColor: "#deeaee",
                borderBottomWidth: 1,
                marginTop:10
              }}
            />
            <FlatList
              data={this.props.timeMovie.showTime}
              numColumns={3}
              renderItem={({ item }) => {
                return (
                  <TouchableOpacity
                    style={{ marginHorizontal: 5, marginTop: 10 }}
                  >
                    <Text style={{ color: "#99ccff", fontSize: 12 }}>
                      <Icon name="sound" size="xxs" color="#99ccff" />
                      {item.soundtrack}
                    </Text>
                    <Button
                      activeStyle={{ backgroundColor: "#80dfff" }}
                      onPress={() => this.goHistory(item)}
                    >
                      <Text style={{ color: "black" }}>
                        {new Date(item.startDateTime)
                          .toLocaleTimeString()
                          .replace(/(.*)\D\d+/, "$1")}
                      </Text>
                    </Button>
                  </TouchableOpacity>
                );
              }}
            />
          </View>

          {/* <View style={{ marginHorizontal: 25, marginVertical: 25 }}>
            
          </View> */}

          <View style={{ marginVertical: 10 }}>
            <WingBlank>
              <Button
                onPress={() => this.props.onDismissDialog()}
                type="warning"
              >
                Cancle
              </Button>
            </WingBlank>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // paddingTop: 50,
    flex: 1
  },

  imageThumbnail: {
    justifyContent: "center",
    alignItems: "center",

    height: 300,
    width: 200,
    borderRadius: 15
  },

  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  modal3: {
    flex: 1,
    position: "absolute",
    top: 20,
    right: 20,
    left: 20,
    bottom: 20,
    backgroundColor: "rgba(0,0,0,0.6)",
    justifyContent: "center",
    alignItems: "center"
  },

  modal4: {
    height: 600
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 22
  }
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Modaly)
);
