import React, { Component } from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text ,TouchableWithoutFeedback } from "react-native";
// import { Icon, TabBar } from "@ant-design/react-native";
import { withRouter } from "react-router-native";
import { Icon, SearchBar, TabBar } from "@ant-design/react-native";

class Menu extends Component {
  goProfile = () => {
    this.props.history.push("/profile");
  };
  goListMovie = () => {
    this.props.history.push("/main");
  };
  goTiket =()=>{
    this.props.history.push("/historyticket");
  }

  render() {
    state = {
      selectedTab: 'redTab',
    };
    
    return (
      <View style={styles.bar}>
        <View style={styles.Row}>
      
          <TouchableOpacity onPress={()=>this.goTiket()}>
            <Icon name="tags" size="lg"  />
            <Text style={{ textAlign: "center" }}>Ticket</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.Row}>
          <TouchableOpacity onPress={this.goListMovie}>
            <Icon name="edit" size="lg" />
            <Text style={{ textAlign: "center" }}>Movie</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.Row}>
          <TouchableOpacity onPress={this.goProfile}>
            <Icon
              name="user"
              size={30}
              disabledStyle={{ backgroundColor: "red" }}
            />
            <Text style={{ textAlign: "center" }}> User</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  Row: {
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 50,
    marginVertical: 5
  },
  bar: {
    flexDirection: "row",
    backgroundColor: "#f3f6f8",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default withRouter(Menu);
