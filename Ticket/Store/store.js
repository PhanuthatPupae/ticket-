import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, combineReducers, compose } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'


import seat from "../Reducers/seatRudercer"
import timeMovie from "../Reducers/MovieTime"
import  movies  from '../Reducers/movieRuducer'
import  user from "../Reducers/UserRuducer"
const reducers = (history) => combineReducers({
    Movies : movies,
    userone: user,
    timeMovie:timeMovie,
    seat : seat,
    router: connectRouter(history)
})

export const history = createMemoryHistory()
export const store = createStore(
    reducers(history), 
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
